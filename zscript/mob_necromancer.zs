// ------------------------------------------------------------
// Necromancer
// ------------------------------------------------------------
class NecromancerGhost:Thinker{
	int targetplayer;
	int ticker;
	bool bfriendly;
	actor ghoster;
	static void Init(actor caller,int time=-1){
		let nnn=new("NecromancerGhost");
		nnn.bfriendly=caller.bfriendly;
		nnn.targetplayer=caller.friendplayer-1;
		nnn.ghoster=caller;
		if(time<0)nnn.ticker=TICRATE*random(60,600);
	}
	override void tick(){
		ticker--;
		if(ticker&(1|2|4|8|16))return;
		if(ticker<=0){
			if(!multiplayer)targetplayer=0;
			else if(
				targetplayer<0
				||targetplayer>=MAXPLAYERS
				||hdspectator(players[targetplayer].mo)
				||!hdplayerpawn(players[targetplayer].mo)
			){
				array<int> pcs;pcs.clear();
				for(int i=0;i<MAXPLAYERS;i++){
					if(
						hdplayerpawn(players[i].mo)
						&&!hdspectator(players[i].mo)
					)pcs.push(i);
				}targetplayer=pcs[random(0,pcs.size()-1)];
			}

			actor pmo=players[targetplayer].mo;
			if(!ticker||!random(0,15)){
				pmo.A_StartSound("vile/active",420,CHANF_UI|CHANF_NOPAUSE|CHANF_LOCAL);
				if(!ticker)return;
			}

			if(!random(0,7)){
				actor aaa;
				blockthingsiterator it=blockthingsiterator.create(pmo,HDCONST_ONEMETRE*5);
				while(it.next()){
					aaa=it.thing;
					if(
						aaa.bcorpse
						&&aaa.findstate("raise")
					){
						HDRaiser.Init(aaa,ghoster,random(35,200),bfriendly);
					}
				}
			}
		}
		if(ticker<(-TICRATE*60))destroy();
	}
}
class HDRaiser:Thinker{
	actor corpse,raiser;
	int ticker,friendplayer;
	bool friendly;
	static HDRaiser Init(
		actor cps,
		actor rsr,
		int time,
		bool friendly
	){
		let hdr=HDRaiser(new("HDRaiser"));
		hdr.corpse=cps;
		hdr.raiser=rsr;
		hdr.ticker=time;
		hdr.friendly=friendly;
		return hdr;
	}
	override void Tick(){
		if(!corpse||corpse.health>0){destroy();return;}
		if(ticker>0)ticker--;
		else{
			if(!!raiser){
				corpse.target=raiser.target;
				corpse.master=raiser;
				if(
					friendly
					&&raiser.player
				)corpse.friendplayer=raiser.playernumber()+1;
			}
			corpse.RaiseActor(corpse,RF_NOCHECKPOSITION);
			corpse.bfriendly=friendly;
			destroy();
		}
	}
}
class NecromancerFlame:HDActor{
	default{
		+nointeraction
		+bright
		renderstyle "add";
		height 0;radius 0;
	}
	override void tick(){
		super.tick();
		let trc=tracer;
		if(!trc)return;
		if(!trc){
			if(!isfrozen())A_FadeOut(0.05);
			return;
		}
		if(trc.isfrozen())return;
		double trd=trc.radius*0.6;
		vector3 npos=trc.pos
			+(
				frandom(-trd,trd),
				frandom(-trd,trd),
				frandom(0.1,trc.height*0.6)
			);
		npos.xy+=(target.pos.xy-trc.pos.xy).unit()*frandom(0,trd);
		setorigin(npos,true);
		scale.y=frandom(0.9,1.1);
		scale.x=frandom(0.9,1.1);
		if(!(level.time&1))scale.x=-scale.x;
	}
	void A_BurnTracer(){
		let trc=tracer;
		if(!trc)return;

		if(
			!target
			||target.bcorpse
			||!checksight(target)
			||absangle(target.angle,target.angleto(trc))>40
		){
			setstatelabel("death");
			return;
		}

		trc.damagemobj(self,target,4,"hot");
		A_Immolate(trc,target,4);
		trc.vel.xy+=(trc.pos.xy-pos.xy).unit()*0.1;
		if(trc.pos.z-trc.floorz<HDCONST_ONEMETRE*2.)trc.vel.z+=(HDCONST_GRAVITY*1.4)*tics;
	}
	states{
	spawn:
		FIRE A 2 light("HELL");
		FIRE B 2 A_StartSound("vile/firecrkl",CHAN_VOICE);
		FIRE ABABCDCDEDEF 2 light("HELL");
		FIRE GHGHGHGH 3 light("HELL") A_BurnTracer();
	death:
		FIRE FGFGH 2 light("HELL");
		FIRE GHFGHGHGH 2 A_FadeOut(0.12);
		stop;
	}
}
class Necromancer:HDMobBase replaces ArchVile{
	override void postbeginplay(){
		super.postbeginplay();

		//spawn shards instead if no archvile sprites
		if(Wads.CheckNumForName("VILER0",wads.ns_sprites,-1,false)<0){
			for(int i=0;i<99;i++){
				actor vvv;
				[bmissilemore,vvv]=A_SpawnItemEx("BFGNecroShard",
					frandom(-3,3),frandom(-3,3),frandom(1,6),
					frandom(0,30),0,frandom(1,12),frandom(0,360),
					SXF_SETMASTER|SXF_TRANSFERPOINTERS|SXF_ABSOLUTEPOSITION
				);
				vvv.A_SetFriendly(bfriendly);
			}
			A_AlertMonsters();
			destroy();
			return;
		}

		bsmallhead=bplayingid;
		resize(0.6,0.8);
		voicepitch=frandom(0.3,1.7);
		settag(RandomName());

		lastshieldcount=countinv("HDMagicShield");
	}
	void A_NecromancerFlame(){
		let tgt=target;
		if(
			tgt
			&&absangle(angle,angleto(tgt))<60
			&&checksight(tgt)
		){
			double rd=tgt.radius*0.6;
			let fff=NecromancerFlame(spawn("NecromancerFlame",
				tgt.pos+(frandom(-rd,rd),frandom(-rd,rd),frandom(0,tgt.height*0.3))
			));
			if(fff){
				fff.scale.x=randompick(-1,1)*frandom(0.9,1.1);
				fff.scale.y=frandom(0.8,1.1);
				fff.target=self;
				fff.tracer=tgt;
			}
			HDBleedingWound.findandpatch(self,10,HDBW_FINDPATCHED);
		}
	}
	void A_NecromancerWarp(){
		if(random(0,3))return;
		spawn("TeleFog",pos);
		bfrightened=true;
		speed*=5;
		maxstepheight*=5;
		maxdropoffheight*=5;
		bsolid=false;
		bnogravity=true;
		bfloat=true;
		bdontinterpolate=true;

		for(int i=0;i<10;i++)A_Chase(null,null);

		HealNearbyCorpse(meleerange);
		HDBleedingWound.findandpatch(self,5,HDBW_FINDPATCHED);

		spawn("TeleFog",pos);
		bfrightened=false;
		speed*=0.2;
		maxstepheight*=0.2;
		maxdropoffheight*=0.2;
		bsolid=true;
		bnogravity=false;
		bfloat=false;
		bdontinterpolate=false;
	}
	override actor HealNearbyCorpse(double healradius){
		let ccc=super.HealNearbyCorpse(meleerange);
		if(!ccc)return null;
		let hdt=HDMobBase(ccc);
		if(hdt){
			hdt.bodydamage>>=5;
			hdt.stunned>>=5;
		}
		int hhh=200-ccc.health;
		if(hhh>0)SetInventory("HDMagicShield",countinv("HDMagicShield")+hhh);
		return ccc;
	}
	override void A_HDChase(
		statelabel meleestate,
		statelabel missilestate,
		int flags,
		double speedmult
	){
		if(
			!HealNearbyCorpse(meleerange)
		)super.A_HDChase(meleestate,missilestate,flags,speedmult);

		if(
			!instatesequence(curstate,resolvestate("heal"))
			&&!random(0,63)
		)A_NecromancerWarp();
	}
	override void CheckFootStepSound(){
		if(
			(
				frame==0
				||frame==3
			)
			&&frame!=curstate.nextstate.frame
		){
			if(HDMath.CheckLiquidTexture(self))A_StartSound("humanoid/squishstep",88,CHANF_OVERLAP,volume:0.2);
			else A_StartSound("imp/step",88,CHANF_OVERLAP,
				volume:(HDMath.CheckDirtTexture(self)?0.1:0.2)
			);
		}
	}
	override void Tick(){
		super.Tick();
		if(isfrozen()||health<1)return;
		if(stunned>100)stunned-=20;
		if(!bplayingid&&(
			(
				frame>=26
				&&frame<=28
			)||(
				frame>5
				&&frame<16
			)
		))ZapArc(self,flags:ARC2_SILENT,radius:radius*4,height:height*1.4);
		if(!(level.time&(1|2))){
			if(bodydamage>0)bodydamage--;
			givebody(1);
		}
	}
	default{
		mass 200;
		maxtargetrange 896;
		seesound "vile/sight";
		painsound "vile/pain";
		deathsound "vile/death";
		activesound "vile/active";
		meleesound "vile/stop";
		obituary "%o met the firepower of $TAG.";
		tag "$CC_ARCH";

		+avoidmelee
		+notarget
		+hdmobbase.chasealert
		+hdmobbase.biped
		+hdmobbase.climber

		radius 16;
		height 56;
		meleerange 64;
		maxstepheight 28;maxdropoffheight 64;
		painchance 42;
		scale 0.8;
		speed 24;
		health 400;
		hdmobbase.shields 1800;
		damagefactor "hot",0.66;
	}
	int lastshieldcount;
	states{
	spawn:
		VILE CD 10 A_HDLook();
		VILE A 0 A_Jump(16,"spwander");
		loop;
	spwander:
		VILE ABCDEF 6 A_HDWander(CHF_LOOK);
		VILE A 0 A_Jump(8,"spawn");
		loop;
	see:
		VILE ABCDEF 3 A_HDChase();
		loop;
	missile:
		VILE A 0 A_Jump(164,2);
		VILE A 0 A_Vocalize(seesound,painsound,deathsound);
		VILE A 0 HealNearbyCorpse(meleerange);
		VILE ABCDEF 3 A_FaceTarget(10,10);
		VILE FG 3 A_FaceTarget(5,5);
		VILE G 3{
			A_FaceTarget(5,5);
			A_StartSound("vile/firestrt",CHAN_WEAPON,CHANF_OVERLAP);
			if(
				target
				&&absangle(angle,angleto(target))<60
				&&checksight(target)
			)target.A_StartSound("vile/firestrt",CHAN_BODY,CHANF_OVERLAP);
		}
		VILE GGHHII 3 bright light("HELL") A_FaceTarget(5,5);
		VILE J 0 A_NecromancerFlame();
		VILE JJJKKKLLL 3 bright light("HELL") A_FaceTarget(5,5);
	missileend:
		VILE L 0 A_StartSound("vile/firestrt",CHAN_WEAPON,CHANF_OVERLAP);
		VILE MMNNOOPP 3 bright light("HELL");
		goto see;
	heal:
		VILE A 0 A_Jump(164,2);
		VILE A 0 A_Vocalize(randompick(activesound,seesound));
		VILE "[\]" 12 bright light("HEAL");
		goto see;
	pain:
		VILE Q 5{
			int shields=countinv("HDMagicShield");
			if(
				lastshieldcount-shields>500
				||!random(0,3)
			)A_NecromancerWarp();
			lastshieldcount=shields;
		}
		VILE Q 5{
			if(target)threat=target;
			A_Vocalize(painsound);
			A_ShoutAlert(0.4,SAF_SILENT);
		}
		goto see;
	xdeath:
		VILE Q 0 A_JumpIf(bplayingid,"idxdeath");
	death:
		VILE Q 0 A_JumpIf(bplayingid,"iddeath");
		VILE QR 6;
		VILE S 5 A_Vocalize(deathsound);
		VILE TUVWXY 4;
		VILE Z 35;
		VILE Z -1{NecromancerGhost.Init(self);}
		stop;


	//id archvile death sequence
	iddeath:
		VILE Q 42 bright{
			A_SetSize(radius,liveheight);
			A_Vocalize(painsound);
			A_UnsetShootable();
			A_FaceTarget();
			accuracy=24;
		}
		VILE Q 0 A_Quake(2,40,0,768,0);
		VILE Q 0 A_SpawnVileDebris(3);
	flicker:
		VILE G 1 bright light("HELL"){
			A_SetRenderStyle(0.8,STYLE_Add);
			A_SetTics((accuracy>>3)+random(0,2));
			if(!random(0,accuracy))A_Vocalize(painsound);
		}
		VILE G 1{
			A_SetRenderStyle(0.4,STYLE_Add);
			A_SetTics((accuracy>>2)+random(0,2));
			accuracy--;
			if(accuracy==20)A_Quake(4,40,0,768,0);
			if(accuracy<1)setstatelabel("flickerend");
		}
		loop;
	flickerend:
		VILE G 0 A_SetRenderStyle(1.,STYLE_Add);
		VILE Q 0 A_Quake(6,8,0,768,0);
		VILE GGG 2 bright light("HELL")A_Vocalize(painsound);
	idxdeath:
		VILE Q 6 bright light("HELL"){
			A_SetSize(radius,liveheight);
			A_Vocalize(painsound);
			A_UnsetShootable();
			A_FaceTarget();

			A_Explode(72,196);
			A_StartSound("weapons/rocklx",CHAN_WEAPON);
			A_SpawnItemEx("NecroDeathLight",flags:SXF_SETTARGET);
			A_Vocalize(deathsound);
			DistantNoise.Make(self,"world/rocketfar",2.);
			A_SpawnVileDebris(3);
		}
		VILE Q 14 bright light("HELL") A_Quake(8,14,0,768,0);
		VILE Q 0 A_SpawnVileDebris();
		VILE Q 2 A_Quake(3,26,0,1024,0);
		VILE Q 2 bright{
			if(
				alpha>0.8
				&&frandom(0,1)>alpha
			)A_SpawnVileDebris();
			else if(frandom(0,1)<alpha)A_SpawnVileDebris(
				1,
				"BossShard",
				1.,
				(angletovector(angle+frandom(135,225),frandom(0.01,0.1)),frandom(0,1))
			);
			A_FadeOut(alpha*0.02);
			if(alpha<0.01){
				NecromancerGhost.Init(self);
				destroy();
			}
		}
		wait;
	}
	void A_SpawnVileDebris(
		int amt=1,
		class<actor> type="VileDeathFire",
		double maxheight=0.6,
		vector3 startvel=(0,0,0)
	){
		double minheight=pos.z;
		maxheight=pos.z+liveheight*maxheight;
		for(int i=0;i<amt;i++){
			actor vdf=spawn(type,(
				pos.xy+(frandom(-radius,radius),frandom(-radius,radius)),
				frandom(minheight,maxheight)
			),ALLOW_REPLACE);
			if(vdf){
				vdf.target=self;
				vdf.vel=startvel;
			}
		}
	}
}

//these two actors are only used by the id archvile.
class VileDeathFire:Actor{
	default{
		+bright
		+nointeraction
		renderstyle "add";
	}
	override void postbeginplay(){
		super.postbeginplay();
		vel.z+=frandom(0.1,2.);
		scale.x=frandom(0.6,1.);
		scale.y=frandom(0.8,1.5);
		if(!random(0,1))scale.x=-scale.x;
	}
	states{
	spawn:
		FIRE ABABCDCDEFEFGHGH 2 A_FadeOut(0.05);
		wait;
	}
}
class NecroDeathLight:PointLight{
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=255;
		args[1]=200;
		args[2]=100;
		args[3]=256;
		args[4]=0;
	}
	override void tick(){
		if(isfrozen())return;
		if(!target||target.bnointeraction){destroy();return;}
		args[3]=int(target.alpha*randompick(1,3,7)*frandom(12,16));
		setorigin(target.pos,true);
	}
}

class GhostlyNecromancer:Necromancer{
	default{
		+noclip
		+floorhugger
		+friendly
		+noblood
		+noblockmonst
		-solid
		-shootable
		renderstyle "add";
		translation "0:255=%[0,0,0]:[0.3,0.7,0.1]";
		speed 10;
	}
	override void CheckFootStepSound(){}
	override void tick(){
		super.tick();
		if(isfrozen())return;

		if(!level.ispointinlevel(pos)){
			for(int i=0;i<MAXPLAYERS;i++){
				if(
					players[i].mo
					&&players[i].mo.health>0
				){
					movepos=players[i].mo.pos;
					break;
				}
			}
			return;
		}

		if(frame>5)alpha=min(alpha+0.1,frandom(0.95,1.));
		else alpha=min(alpha+frandom(!!target?-0.099:-0.101,0.1),0.8);

		if(!(level.time&(1|2|4))){
			A_Trail();
		}

		if(alpha<-1.){
			static const string msg[]={
				"My work here is done.",
				"There is nothing here.",
				"I see no further need here for my intervention.",
				"Goodbye.",
				"I ascend.",
				"Nothing to be done.",
				"Logging off.",
				"I descend.",
				"Good luck finding the exit.",
				"So long, and thanks for all the fish.",
				"Now where did I leave that cheesecake..."
			};
			for(int i=0;i<MAXPLAYERS;i++){
				if(players[i].mo)players[i].mo.A_StartSound("misc/chat",420,CHANF_UI|CHANF_NOPAUSE|CHANF_LOCAL);
			}
			console.printf("\cd"..gettag()..": "..msg[random(0,random(0,msg.size()-1))]);
			destroy();
		}
	}
}




//old version below

// 2022-05-05 MC: The rewrite was prompted after a long history of desyncs caused by the old code. I could never find the source - everything appeared to be well within the playsim and should not have differed at all between players. It did seem to be strongly associated with the unique damage system and going into pain state, however.

/*

class BloodyHellFire:HDMobBase{
	default{
		+noblood
		+bright
		+lookallaround
		+frightening
		+dontgib
		+nofear
		+float
		-countkill
		-solid
		-shootable
		renderstyle "add";
		gravity 0.1;
		speed 10;
		minmissilechance 16;
		health 120;
		damagefactor "hot",0;
		meleerange 48;
		radius 12;
		height 16;
		damage 24;
		maxstepheight 128;
		tag "$CC_FLAME";
	}
	override void postbeginplay(){
		super.postbeginplay();
		A_GiveInventory("ImmunityToFire");
	}
	override void tick(){
		super.tick();
		if(
			!isfrozen()
			&&!(level.time&(1|2|4))
			&&bshootable
		){
			A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP);
			if(!(level.time&(1|2|4|8))){
				A_SpawnItemEx("HDSmoke",frandom(-1,1),frandom(-1,1),frandom(1,6),frandom(-1,1),frandom(-1,1),frandom(1,3),0,SXF_ABSOLUTE,64);
				A_Trail();
				damagemobj(null,null,3,"maxhpdrain");
			}
		}
	}
	states{
	spawn:
		TNT1 A 0 A_Jump(256,"idle");
		TNT1 AAAAAAA random(4,6) A_SpawnItemEx("HDSmoke",frandom(-1,1),frandom(-1,1),frandom(1,6),frandom(-1,1),frandom(-1,1),frandom(1,3),0,SXF_ABSOLUTE);
		TNT1 AAAAAAA random(1,4) A_SpawnItemEx("HDSmoke",frandom(-1,1),frandom(-1,1),frandom(1,6),frandom(-1,1),frandom(-1,1),frandom(1,3),0,SXF_ABSOLUTE);
		TNT1 A 0 A_StartSound("vile/firestrt",CHAN_VOICE,CHANF_OVERLAP);
		TNT1 A 0 A_JumpIf(
			master
			&&(
				!master.target
				||master.target.countinv("ImmunityToFire")
				||(
					!master.checksight(master.target)
					&&!random(0,3)
				)
			)
		,"putto");
		TNT1 A 0 A_SetShootable();
		TNT1 A 0 A_SpawnItemEx("NecroFireLight",flags:SXF_SETTARGET);
		TNT1 AAAAA 0 A_SpawnItemEx("HDSmoke",frandom(-1,1),frandom(-1,1),frandom(1,6),frandom(-1,1),frandom(-1,1),frandom(1,3),0,SXF_ABSOLUTE);
	idle:
		FIRE A 2 A_HDWander(CHF_LOOK);
		FIRE BABAB 2;
		loop;
	see:
		FIRE A 1 A_HDChase();
		FIRE BAB 1;
		loop;
	missile:
		FIRE A 1 A_FaceTarget();
		FIRE B 1{
			A_Recoil(-1);
			if(!random(0,15))A_SpawnProjectile("HeckFire",16,0,8);
		}
		goto see;
	melee:
		FIRE ABABABCBCBCDCDCDEDEFGFGFGFGHGHGH 2 A_VileFireMelee();
		stop;
	heal:
		FIRE ABABCBCDCDEDEFGFGFGHGH 1;
		stop;
	death:
		FIRE A 2{
			bnointeraction=true;
			bnofear=true;
			A_QuakeEx(2,2,5,20,0,256,"",QF_SCALEDOWN|QF_WAVE,10,10,10);
			A_HDBlast(pushradius:128,pushamount:64,source:master);
			A_Immolate(null,master,80);
			A_StartSound("misc/fwoosh",CHAN_BODY,CHANF_OVERLAP);
		}
		FIRE AABBCCDEFGH 2{
			let aaa=spawn("HDFlameRedBig",(pos.xy,pos.z+frandom(10,40)),ALLOW_REPLACE);
			if(aaa){
				aaa.target=master;
				aaa.vel=vel+(frandom(-1,1),frandom(-1,1),frandom(0.1,2));
			}
		}
		stop;
	putto:
		TNT1 A 0{
			let hdmm=hdmobbase(master);
			if(hdmm)hdmm.firefatigue+=HDCONST_MAXFIREFATIGUE;
		}
		TNT1 AAAAA 0 A_SpawnItemEx("HDSmoke",frandom(-1,1),frandom(-1,1),frandom(1,6),frandom(-1,1),frandom(-1,1),frandom(1,3),0,SXF_ABSOLUTE);
		TNT1 A 0 A_SpawnItemEx("Putto",flags:SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS);
		stop;
	}
	double lasttargetangle;
	void A_VileFireMelee(){
		if(!target)return;
		setorigin((
			target.pos.xy+angletovector(target.angle,frandom(1,7)),
			target.pos.z+target.height*0.7
			),true
		);
		vel=(vel+target.vel)*0.5;
		A_StartSound("misc/firecrkl",CHAN_VOICE,CHANF_OVERLAP);
		A_Immolate(target,master,2);
		HDF.Give(target,"Heat",6);
		target.vel.z+=tics*HDCONST_GRAVITY+0.04;

		if(
			target.health>0
			&&absangle(lasttargetangle,target.angle)>frandom(0,500)
		)target=null;
		else lasttargetangle=target.angle;
	}
}
class NecroFireLight:PointLight{
	bool triggered;
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=200;
		args[1]=160;
		args[2]=70;
		args[3]=64;
		args[4]=0;
		triggered=false;
	}
	override void tick(){
		if(!target||target.bnointeraction){destroy();return;}
		setorigin(target.pos,true);
		if(!target.bnofear){
			if(!triggered){
				args[3]=128;
				triggered=true;
			}else args[3]=int(frandom(0.8,1.)*args[3]);
		}
		else args[3]=random(50,72);
	}
}

class HeckFire:HDActor{
	default{
		projectile;
		-nogravity
		+hittracer
		+slidesonwalls
		+noexplodefloor
		+seekermissile
		+nodamagethrust
		+bloodlessimpact
		+bright
		-noblockmap
		gravity 0.6;
		damagetype "hot";
		renderstyle "add";
		alpha 0.7;
		speed 12;
		radius 8;
		height 16;
	}
	states{
	spawn:
		FIRE ABAB random(1,2);
		TNT1 A 0 A_Gravity();
		FIRE ABAB random(1,2) A_SeekerMissile(2,4);
	see:
		FIRE A 0 A_Setscale(randompick(-1,1)*frandom(0.8,1.2),frandom(0.8,1.2));
		FIRE A random(1,2) A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP);
		FIRE B random(1,2) A_NoGravity();
		FIRE A 0 A_Setscale(randompick(-1,1)*frandom(0.8,1.2),frandom(0.8,1.2));
		FIRE ABAB random(1,2) A_SeekerMissile(4,8);
		FIRE A 0 A_Setscale(randompick(-1,1)*frandom(0.8,1.2),frandom(0.8,1.2));
		FIRE A random(1,2) A_Jump(24,2);
		FIRE B random(1,2) A_Gravity();
		loop;
	death:
	fade:
		FIRE CDCDEFEFGH 2;
		stop;
	xdeath:
		FIRE C 1{
			A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP);
			if(tracer)A_Immolate(tracer,target?target.master:null,1);
		}
		FIRE DCDCB 1{
			if(!tracer)return;
			A_Face(tracer);
			A_Recoil(-speed*0.3);
		}
		FIRE CBABAB 1;
		FIRE B 0 A_Jump(8,"see");
		FIRE B 0{
			A_StartSound("misc/firecrkl",CHAN_BODY,CHANF_OVERLAP);
			if(tracer)bmissile=true;
		}
		goto see;
	}
}

class NecromancerOld:HDMobBase{
	string nickname;
	default{
		mass 500;
		maxtargetrange 896;
		seesound "vile/sight";
		painsound "vile/pain";
		deathsound "vile/death";
		activesound "vile/active";
		meleesound "vile/stop";
		obituary "%o met the firepower of the $TAG.";
		tag "$CC_ARCH";
		
		+shadow
		+nofear
		+seeinvisible
		+frightening
		+dontgib
		+floorclip
		+hdmobbase.doesntbleed
		+hdmobbase.biped
		+hdmobbase.chasealert
		+nopain
		radius 16;
		height 56;
		scale 0.8;
		renderstyle "normal";
		bloodcolor "ff ff 44";
		damagetype "hot";
		speed 14;
		painchance 0;
		health 1000;
	}
	static void A_MassHeal(actor caller){
		actor aaa;
		blockthingsiterator it=blockthingsiterator.create(caller,256);
		while(it.next()){
			aaa=it.thing;
			if(
				aaa.bcorpse
				&&aaa.findstate("raise")
				&&aaa.checksight(caller)
			){
				int dist=int(max(
					abs(aaa.pos.x-caller.pos.x),
					abs(aaa.pos.y-caller.pos.y),
					abs(aaa.pos.z-caller.pos.z)
				));
				actor hhh=spawn("HDRaiser",aaa.pos,ALLOW_REPLACE);
				hhh.A_SetFriendly(caller.bfriendly);
				hhh.friendplayer=caller.friendplayer;
				hhh.master=aaa;
				hhh.tracer=caller;
				hhh.stamina=(dist>>3)+random(10,30);
			}
		}
	}
	override void beginplay(){
		hitsleft=bfriendly?7:6;
		super.beginplay();
	}
	override void postbeginplay(){
		super.postbeginplay();
		if(
			!bfriendly
			&&hd_novilespam
			&&A_CheckProximity("null","Necromancer",2018,2,CPXF_NOZ)
		){
			A_Die("mapmorph");
			return;
		}

		voicepitch=1.+frandom(-1.,1.);

		//spawn shards instead if no archvile sprites
		if(Wads.CheckNumForName("VILER0",wads.ns_sprites,-1,false)<0){
			for(int i=0;i<99;i++){
				actor vvv;
				[bmissilemore,vvv]=A_SpawnItemEx("BFGNecroShard",
					frandom(-3,3),frandom(-3,3),frandom(1,6),
					frandom(0,30),0,frandom(1,12),frandom(0,360),
					SXF_SETMASTER|SXF_TRANSFERPOINTERS|SXF_ABSOLUTEPOSITION
				);
				vvv.A_SetFriendly(bfriendly);
			}
			A_AlertMonsters();
			destroy();
			return;
		}

		bsmallhead=bplayingid;

		nickname=RandomName();
		settag(nickname);

		resize(0.8,1.3);
		A_GiveInventory("ImmunityToFire");
	}
	int hitsleft;
	void A_ChangeNecroFlags(bool attacking){
		if(!attacking){
			A_UnSetShootable();
			A_UnSetSolid();
			bnofear=false;
			bfrightened=true;
			maxstepheight=1024;
			maxdropoffheight=1024;
			A_SetRenderStyle(1.,STYLE_Add);
		}else{
			A_SetShootable();
			A_SetSolid();
			bnofear=true;
			bfrightened=false;
			maxstepheight=default.maxstepheight;
			maxdropoffheight=default.maxdropoffheight;
			A_SetRenderStyle(1.,STYLE_Normal);
		}
	}
	override int damagemobj(
		actor inflictor,actor source,int damage,
		name mod,int flags,double angle
	){
		if(
			damage==TELEFRAG_DAMAGE
			||mod=="mapmorph"
		)return actor.damagemobj(inflictor,source,damage,mod,flags,angle);

		if(
			(
				(
					mod!="hot"
					&&mod!="balefire"
				)||(
					source
					&&source.target==self
					&&(
						Putto(source)
						||Necromancer(source)
					)
				)
			)
			&&damage>random(0,bfriendly?333:166)
		){
			if(hitsleft>0){
				if(!target)target=source;
				setstatelabel("pain");
			}else return actor.damagemobj(
				inflictor,source,health,
				mod,DMG_FORCED|DMG_THRUSTLESS
			);
		}
		return -1;
	}
	states{
	spawn:
		VILE A 0 A_JumpIf(!bshootable,"painedandgone");
		VILE AB 10 A_HDLook();
		loop;
	see:
		VILE A 0 A_JumpIf(!bfriendly,2);
		VILE H 0 A_ClearTarget();
		VILE ABCDEF 5 A_HDChase();
		loop;
	missile:
		VILE G 10 bright{
			bnopain=false;
			A_FaceTarget();
		}
		VILE H 2 bright light("CANF") A_ChangeNecroFlags(true);
		VILE HHH 3 bright light("CANF");
		TNT1 A 0 A_FaceTarget();
		VILE IJ 4 bright light("CANF");
		VILE KKL 4 bright light("HECK");
		VILE L 8 bright light("HELL") A_FaceTarget();
		VILE MN 2 bright light("HELL");
		TNT1 A 0 A_FaceTarget();
		VILE NOO 2 bright;
		VILE P 0{
			A_FaceTarget();
			let ppp=spawn("BloodyHellFire",lasttargetpos,ALLOW_REPLACE);
			if(ppp){
				ppp.master=self;
				ppp.target=target;
				ppp.A_SetFriendly(bfriendly);
				ppp.friendplayer=friendplayer;
				firefatigue+=HDCONST_MAXFIREFATIGUE;
			}
		}
		VILE PO 4 bright light("HELL");
		VILE N 8 bright light("HELL");
		---- A 0 setstatelabel("see");
	heal:
		VILE A 0 A_ChangeNecroFlags(true);
		VILE \\ 8 bright light("HEAL");
		VILE # 8 bright light("HEAL"){
			if(bfriendly){
				hitsleft=7;
				flinetracedata hlt;
				linetrace(
					angle,radius*3,0,
					flags:TRF_THRUBLOCK|TRF_THRUHITSCAN|TRF_ALLACTORS,
					offsetz:8,
					data:hlt
				);
				if(hlt.hitactor){
					let hdmb=hdmobbase(hlt.hitactor);
					if(hdmb)hdmb.bodydamage=0;
					hlt.hitactor.A_SetFriendly(bfriendly);
				}
			}else{
				A_MassHeal(self);
				A_SetTics(16);
			}
		}
		---- A 0 setstatelabel("see");
	pain:
		VILE Q 20 light("HELL"){
			hitsleft--;
			A_UnsetShootable();
			if(!bfriendly)A_ChangeNecroFlags(false);

			DistantNoise.Make(self,"world/rocketfar");
			A_SpawnItemEx("SpawnFire",0,0,28,flags:SXF_NOCHECKPOSITION);
			A_Explode(46,196);
			A_Quake(3,36,0,360);
			A_AlertMonsters();
			A_Vocalize(painsound);
			for(int i=0;i<3;i++)A_SpawnItemEx("BFGNecroShard",
				0,0,42,flags:SXF_SETMASTER|SXF_TRANSFERPOINTERS
			);
		}
		VILE H 0 A_JumpIf(bfriendly,"see");
	pained:
		VILE A 1 A_Chase(null,null);
		VILE AA 0 A_Chase(null,null);
		VILE B 1 A_Chase(null,null);
		VILE AA 0 A_Chase(null,null);
		VILE C 1 A_Chase(null,null);
		VILE AA 0 A_Chase(null,null);
		VILE F 0 A_SetTranslucent(alpha-0.2,1);
		VILE D 1 A_Chase(null,null);
		VILE AA 0 A_Chase(null,null);
		VILE E 1 A_Chase(null,null,CHF_RESURRECT);
		VILE AA 0 A_Chase(null,null,CHF_RESURRECT);
		VILE F 1 A_Chase(null,null,CHF_RESURRECT);
		VILE AA 0 A_Chase(null,null,CHF_RESURRECT);
		VILE F 0 A_SetTranslucent(alpha-0.2,1);
		VILE F 0 A_JumpIf(alpha<0.1,"painedandgone");
		loop;
	painedandgone:
		TNT1 AAAAAAAAA 0 A_Wander();
		TNT1 A 0 A_SetTics(random(350,3500));
		VILE F 0 A_ChangeNecroFlags(true);
		---- A 0 setstatelabel("see");
	death:
		VILE Q 0 A_ChangeNecroFlags(false);
		VILE Q 42 bright A_Vocalize(painsound);
		VILE Q 0 A_FaceTarget();
		VILE Q 0 A_Quake(2,40,0,768,0);
		VILE G 6 bright light("HELL") A_SetTranslucent(0.8,1);
		VILE G 6 A_SetTranslucent(0.4,1);
		VILE G 6 bright light("HELL") A_SetTranslucent(0.8,1);
		VILE G 6 A_SetTranslucent(0.4,1);
		VILE G 6 bright light("HELL") A_Vocalize(painsound);
		VILE Q 0 A_Quake(4,40,0,768,0);
		VILE G 4 bright light("HELL") A_SetTranslucent(0.8,1);
		VILE G 4 A_SetTranslucent(0.4,1);
		VILE G 4 bright light("HELL") A_SetTranslucent(0.8,1);
		VILE G 4 A_SetTranslucent(0.4,1);
		VILE G 4 bright light("HELL") A_Vocalize(painsound);
		VILE G 2 bright light("HELL") A_SetTranslucent(1,1);
		VILE G 2 A_SetTranslucent(0.5,1);
		VILE G 2 bright light("HELL") A_SetTranslucent(1,1);
		VILE G 2 A_SetTranslucent(0.5,1);
		VILE G 0 A_SetTranslucent(1,1);
		VILE Q 0 A_Quake(6,8,0,768,0);
		VILE GGG 2 bright light("HELL")A_Vocalize(painsound);
	xdeath:
		VILE Q 6 bright light("HELL"){
			A_Explode(72,196);
			A_StartSound("weapons/rocklx",CHAN_WEAPON);
			A_SpawnItemEx("NecroDeathLight",flags:SXF_SETTARGET);
			A_SpawnItem("spawnFire",0.1,28,0,0);
			A_Vocalize(deathsound);
			DistantNoise.Make(self,"world/rocketfar",2.);
			A_ChangeNecroFlags(false);
		}
		VILE Q 14 bright light("HELL") A_Quake(8,14,0,768,0);
		VILE QQQQQQ 0 A_SpawnItemEx("SpawnFire",
			frandom(-3,3),frandom(-3,3),frandom(25,30),
			frandom(-3,3),frandom(-3,3),frandom(0.5,4),
			flags:SXF_ABSOLUTE
		);
		VILE Q 0 A_Quake(3,26,0,1024,0);
		VILE QQQQQ 8 A_SpawnItemEx("NecroShard",frandom(-8,8),frandom(-8,8),frandom(26,50),frandom(-1,1),0,0,frandom(0,360),SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS);
	fade:
		VILE QQ 2 A_SpawnItemEx("NecroShard",frandom(-8,8),frandom(-8,8),frandom(26,50),frandom(-1,1),0,0,frandom(0,360),SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS,16);
		VILE Q 8 A_fadeOut(0.05);
		VILE Q 0 A_JumpIf(alpha < 0.10,1);
		loop;
		VILE Q 8 A_fadeOut(0.04);
		TNT1 A 8 A_SpawnItemEx("NecroShard",frandom(-8,8),frandom(-8,8),frandom(26,50),frandom(-1,1),0,0,frandom(0,360),SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS,16);
		TNT1 A 0 A_NoBlocking();
		TNT1 AAAAA 12 A_SpawnItemEx("NecroShard",frandom(-8,8),frandom(-8,8),frandom(26,50),frandom(-1,1),0,0,frandom(0,360),SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS,16);
	dead:
		TNT1 A 20;
		TNT1 A 0 A_Jump(1,"ghost");
		loop;
	ghost:
		TNT1 A 20 A_Warp(AAPTR_TARGET,0,0,32,0,WARPF_NOCHECKPOSITION);
		TNT1 A 80 A_Quake(1,40,0,512,"vile/curse");
		TNT1 AAAAAA 0 A_SpawnItemEx("NecroGhostShard",flags:SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
		TNT1 A 40;
		TNT1 A 1 A_MassHeal(self);
		stop;

	death.mapmorph:
		TNT1 A 0 A_Jump(128,"rep_np","rep_baron");
	rep_imp:
		TNT1 AAAA 0 A_SpawnItemEx("Regentipede",flags:SXF_NOCHECKPOSITION|SXF_TRANSFERAMBUSHFLAG);
		stop;
	rep_np:
		TNT1 AAA 0 A_SpawnItemEx("NinjaPirate",flags:SXF_NOCHECKPOSITION|SXF_TRANSFERAMBUSHFLAG);
		stop;
	rep_baron:
		TNT1 A 0 A_SpawnItemEx("PainLord",flags:SXF_NOCHECKPOSITION|SXF_TRANSFERAMBUSHFLAG);
		TNT1 AA 0 A_SpawnItemEx("PainBringer",flags:SXF_NOCHECKPOSITION|SXF_TRANSFERAMBUSHFLAG);
		stop;
	}
}
class NecroDeathLight:PointLight{
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=255;
		args[1]=200;
		args[2]=100;
		args[3]=256;
		args[4]=0;
	}
	override void tick(){
		if(isfrozen())return;
		if(!target||target.bnointeraction){destroy();return;}
		args[3]=int(target.alpha*randompick(1,3,7)*frandom(12,16));
		setorigin(target.pos,true);
	}
}
class LightBearer:Necromancer{
	default{
		+friendly
		+noclip
		+noblockmonst
		maxstepheight 4096;
		maxdropoffheight 4096;
		painchance 56;
		scale 1.0;
		speed 18;
		species "LightBearer";
	}
}

class NecroGhostShard:NecroShard{
	default{
		+ismonster
		-countkill
		+noclip
		+nogravity
		+lookallaround
		+nosector
		+nofear
		height 56;
		radius 20;
		speed 20;
	}
	states{
	spawn:
		TNT1 A 2 A_VileChase();
		loop;
	heal:
		TNT1 A 1{NecromancerOld.A_MassHeal(self);}
		stop;
	}
}
class NecroShard:HDActor{
	default{
		+ismonster
		+float
		+nogravity
		+noclip
		+lookallaround
		translation 1;
		scale 0.6;
		radius 0;
		height 0;
		renderstyle "add";
		speed 5;
	}
	states{
	spawn:
		APLS A 0;
		APLS A 0 A_SetGravity(0.1);
		APLS AB 1 bright A_Look();
		loop;
	see:
		APLS A 0 A_Jump(64,2);
		APLS A 0 {vel.z+=frandom(-4,8);}
		APLS A 2 bright A_JumpIf(alpha<0.1,"null");
		APLS B 2 A_Wander();
		APLS A 0 A_SpawnProjectile("NecroShardTail",0,random(-24,24),random(-24,24),2,random(-14,14));
		APLS A 2 bright;
		APLS A 0 A_Wander();
		APLS B 2 bright A_fadeOut(0.05);
		APLS A 0 A_Wander();
		loop;
	heal:
		TNT1 A 1;
		stop;
	}
}
class NecroShardTail:HDActor{
	default{
		projectile;
		+noclip
		+nogravity
		speed 2;
		scale 0.4;
		translation 1;
		renderstyle "add";
	}
	states{
	spawn:
		APLS AB 2 bright nodelay A_fadeOut(0.01);
		APLS A 0 ThrustThingZ(0,random(-4,8),0,0);
		loop;
	}
}

*/

